﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convo
{
    public class Pedido
    {
        public Pedido(int numPedido, bool contentLoaded)
        {
            NumPedido = numPedido;
            _contentLoaded = contentLoaded;
        }

        public int NumPedido { get; set; }

    }
}
